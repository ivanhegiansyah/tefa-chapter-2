import React from 'react';
import Header from '../elements/Header/Header';
import Sidebar from '../elements/Sidebar/Sidebar';

export default function Layouts({ children }) {
  return (
    <div className="flex">
      <aside className="w-52 min-h-screen">
        <Sidebar />
      </aside>

      <main className={`flex-1`}>
        <Header />
        <div className="p-6">{children}</div>
      </main>
    </div>
  );
}
