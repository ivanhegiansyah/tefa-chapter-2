import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scoped.css';

export default function Button({
  color,
  text,
  className,
  onClick,
  icon,
  size,
}) {
  const classes = [styles.root, styles[color], styles[size], className]
    .filter(Boolean)
    .join(' ');
  return (
    <button className={classes} onClick={onClick}>
      <img alt="" src={icon} />
      {text}
    </button>
  );
}
Button.propTypes = {
  className: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  size: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};
