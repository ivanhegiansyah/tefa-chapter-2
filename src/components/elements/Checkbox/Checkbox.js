import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.scoped.css';

export default function Checkbox({ onChange, text, isChecked }) {
  return (
    <label>
      <input className={styles.input} onChange={onChange} type="checkbox" />
      <svg
        aria-hidden="true"
        className={`${styles.checkbox} ${
          isChecked ? styles['checkbox--active'] : ''
        }`}
        fill="none"
        viewBox="0 0 15 11"
      >
        <path
          d="M1 4.5L5 9L14 1"
          stroke={isChecked ? '#fff' : 'none'}
          strokeWidth="2"
        />
      </svg>
      {text}
    </label>
  );
}

Checkbox.propTypes = {
  isChecked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};
