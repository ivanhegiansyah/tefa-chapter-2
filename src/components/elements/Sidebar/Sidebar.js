import React from 'react';
import logo from '../../../assets/Logo.svg';
import productIcon from '../../../assets/productIcon.svg';
import userIcon from '../../../assets/userIcon.svg';
import { NavLink } from 'react-router-dom';

export default function Sidebar() {
  return (
    <div className="bg-[#1E293B] text-[#E2E8F0] w-52 px-6 py-3 fixed h-full">
      <img alt="" className="ml-2" src={logo} />
      <div className="text-[#64748B] mt-12 mb-5 ml-2">PAGES</div>
      <ul>
        <li>
          <NavLink
            className={(param) =>
              param.isActive
                ? 'flex items-center bg-[#0F172A] px-3 py-2 rounded'
                : 'flex items-center px-3 py-2 rounded'
            }
            to="/products"
          >
            <img className="mr-2" src={productIcon} />
            <p>Products</p>
          </NavLink>
        </li>
        <li>
          <NavLink
            className={(param) =>
              param.isActive
                ? 'flex items-center bg-[#0F172A] px-3 py-2 rounded mt-1'
                : 'flex items-center px-3 py-2 rounded mt-1'
            }
            to="/users"
          >
            <img className="mr-2" src={userIcon} />
            <p>Users</p>
          </NavLink>
        </li>
      </ul>
    </div>
  );
}
