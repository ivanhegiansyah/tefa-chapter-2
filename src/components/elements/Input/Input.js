import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scoped.css';

export default function Input({
  area,
  className,
  name,
  disabled,
  text,
  value,
  onChange,
  labelClassName,
  type,
  placeholder,
  width,
  accept,
}) {
  const classes = [
    styles.root,
    styles[width],
    styles[labelClassName],
    className,
  ]
    .filter(Boolean)
    .join(' ');

  const areaField = (
    <textarea
      id={name}
      onChange={onChange}
      placeholder={placeholder}
      rows="4"
      type={type || 'text'}
      value={value}
    />
  );

  const inputField = (
    <input
      accept={accept || '*'}
      disabled={disabled}
      id={name}
      onChange={onChange}
      placeholder={placeholder}
      type={type || 'text'}
      value={value}
    />
  );

  return (
    <div className={classes}>
      <label className={labelClassName} htmlFor={name}>
        {text}
      </label>
      {area ? areaField : inputField}
    </div>
  );
}

Input.propTypes = {
  accept: PropTypes.string.isRequired,
  area: PropTypes.bool.isRequired,
  className: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  labelClassName: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
};
