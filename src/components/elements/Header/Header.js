import React from 'react';
import arrowIcon from '../../../assets/arrowIcon.svg';
import profileIcon from '../../../assets/profileIcon.svg';
import warningIcon from '../../../assets/warningIcon.svg';
import messageIcon from '../../../assets/messageIcon.svg';
import searchIcon from '../../../assets/searchIcon.svg';

export default function Header() {
  return (
    <div className="flex justify-end bg-white p-4">
      <img alt="" className="mr-2" src={searchIcon} />
      <img alt="" className="mr-2" src={messageIcon} />
      <img alt="" src={warningIcon} />
      <div className="border-r-2 mx-3" />
      <div className="flex justify-center items-center">
        <img alt="" src={profileIcon} />
        <div className="ml-1 mr-5 font-thin">Acne</div>
        <img alt="" g src={arrowIcon} />
      </div>
    </div>
  );
}
