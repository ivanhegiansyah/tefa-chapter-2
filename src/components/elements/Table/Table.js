import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal/Modal';

export default function Table({
  deleteProduct,
  items,
  columns,
  rows,
  showModal,
  setShowModal,
  textModal,
}) {
  return (
    <div>
      <Modal
        deleteProduct={deleteProduct}
        handleClose={() => {
          setShowModal(false);
        }}
        show={showModal}
        text={textModal}
      />
      <div className="w-full border-t border-[#F1F5F9]" />
      <div className="p-5">
        <table className="table-auto w-full text-left">
          <thead className="bg-[#F8FAFC] text-[#9CA3AF]">
            <tr>
              {columns.map((i, idx) => (
                <th className="px-5 py-8 font-normal" key={idx}>
                  {i}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>{items?.map(rows)}</tbody>
        </table>
      </div>
    </div>
  );
}

Table.propTypes = {
  columns: PropTypes.array.isRequired,
  deleteProduct: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  rows: PropTypes.func.isRequired,
  setShowModal: PropTypes.bool.isRequired,
  showModal: PropTypes.string.isRequired,
  textModal: PropTypes.string.isRequired,
};
