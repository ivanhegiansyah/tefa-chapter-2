import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scoped.css';

export default function Breadcrumbs({ items }) {
  return (
    <nav className={styles.root}>
      <div className={styles.first}>{Object.values(items[0])}</div>
      <div>/</div>
      <div>{Object.values(items[1])}</div>
    </nav>
  );
}

Breadcrumbs.propTypes = {
  items: PropTypes.array.isRequired,
};
