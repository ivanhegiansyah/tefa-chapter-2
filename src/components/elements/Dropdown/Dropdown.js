import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scoped.css';

export default function Dropdown({ onChange, name, data, value }) {
  return (
    <div className={styles['select-wrapper']}>
      <select
        className={styles['input-field__input']}
        id={name}
        name={name}
        onChange={onChange}
        required
        value={value}
      >
        <option defaultValue hidden value="">
          Select Category
        </option>
        {data.map((item, i) => (
          <option key={i} value={item}>
            {item}
          </option>
        ))}
      </select>
    </div>
  );
}

Dropdown.propTypes = {
  data: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};
