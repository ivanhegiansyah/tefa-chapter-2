import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';

export default function Modal({ handleClose, show, text, deleteProduct }) {
  return (
    <div
      className={
        show
          ? 'justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none'
          : 'hidden'
      }
    >
      <div className="relative w-auto my-6 mx-auto">
        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
          <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200">
            <h3 className="text-2xl font-semibold">Hapus {text}?</h3>
          </div>
          <div className="flex items-center gap-4 justify-end p-6 border-t border-solid border-slate-200">
            <Button
              color="white"
              onClick={handleClose}
              size="small"
              text={'Close'}
            />
            <Button
              color="red"
              onClick={() => {
                deleteProduct();
                handleClose();
              }}
              size="small"
              text={'Delete'}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  deleteProduct: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
};
