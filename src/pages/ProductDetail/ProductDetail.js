import React from 'react';
import { useParams } from 'react-router-dom';

export default function ProductDetail() {
  const { id } = useParams('id');
  return <h1 className="text-2xl">{id}</h1>;
}
