import React, { useState } from 'react';
import data from './user.json';
import Table from '../../components/elements/Table';
import deleteIcon from '../../assets/deleteIcon.svg';
import Button from '../../components/elements/Button/Button';

export default function UserList() {
  const [showModal, setShowModal] = useState(false);
  const [textModal, setTextModal] = useState('');

  const columns = ['USERNAME', 'EMAIL', 'ACTION'];
  const rows = (i, idx) => {
    return (
      <tr className="border-b-2 border-[#F1F5F9]" key={idx}>
        <td className="p-5">
          <div className="flex items-center">
            <img
              alt=""
              className="rounded-full w-20 mr-4"
              src={i.profilePicture}
            />
            <div>{i.username}</div>
          </div>
        </td>
        <td className="p-5">{i.email}</td>
        <td className="p-5">
          <Button
            color="red"
            icon={deleteIcon}
            onClick={() => {
              setTextModal(i.name);
              setShowModal(true);
            }}
            size="small"
            text={'Delete'}
          />
        </td>
      </tr>
    );
  };
  return (
    <div className="bg-white">
      {/* <select onChange={onChangeSelect} value={category}>
        <option value="">All Category</option>
        <option value="Pre-Order">Pre-Order</option>
        <option value="Ready">Ready</option>
        <option value="Barang Bekas">Barang Bekas</option>
      </select> */}
      <div className="flex justify-between items-center  px-5 py-10">
        <h6 className="font-semibold text-xl">Users</h6>
      </div>
      <Table
        columns={columns}
        isProduct={true}
        items={data}
        rows={rows}
        setShowModal={setShowModal}
        showModal={showModal}
        textModal={textModal}
      />
    </div>
  );
}
