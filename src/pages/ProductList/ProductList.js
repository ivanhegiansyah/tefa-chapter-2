import React, { useState, useEffect } from 'react';
import defaultData from './product.json';
import { useNavigate, useLocation } from 'react-router-dom';
import Table from '../../components/elements/Table';
import Checkbox from '../../components/elements/Checkbox/Checkbox';
import deleteIcon from '../../assets/deleteIcon.svg';
import Button from '../../components/elements/Button/Button';
import Dropdown from '../../components/elements/Dropdown/Dropdown';

export default function ProductList() {
  const navigate = useNavigate();
  const { state } = useLocation();
  const [data, setData] = useState(defaultData);
  const [category, setCategory] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [textModal, setTextModal] = useState('');
  const [isChecked, setIsChecked] = useState(false);
  const [idxDel, setIdxDel] = useState('');

  let today = new Date().toISOString().slice(0, 10);

  const defaultSelect = 'All Category';
  const listCategory = ['All Category', 'Pre-Order', 'Barang Bekas', 'Ready'];
  const columns = [
    'Product Name',
    'Description',
    'Product Price',
    'Category',
    'Expiry Date',
    'ACTION',
  ];

  const filteredData =
    !category || category === defaultSelect
      ? data
      : data.filter((i) => i.category === category);

  const onChangeSelect = (e) => {
    setCategory(e.target.value);
  };

  const onChangeCheck = () => {
    setIsChecked(!isChecked);
    if (!isChecked) {
      const filteredData = data.filter(
        (i) => i.expiryDate > today || i.expiryDate === null
      );
      setData(filteredData);
    } else {
      setData(defaultData);
    }
  };

  const inputProduct = () => {
    setData([...data, state]);
  };

  const deleteProduct = () => {
    const product = data.filter((item) => item.id !== idxDel);
    setData([...product]);
  };

  const rows = (i, idx) => {
    return (
      <tr
        className={
          i.expiryDate < today
            ? 'border-b-2 border-[#F1F5F9] opacity-50'
            : 'border-b-2 border-[#F1F5F9]'
        }
        key={idx}
      >
        <td
          className="p-5 cursor-pointer"
          onClick={() => {
            navigate(`/products/${i.id}`);
          }}
        >
          {i.name}
        </td>
        <td className="p-5 w-96">{i.description}</td>
        <td className="p-5 text-[#10B981]">
          {i.price.toLocaleString('id-ID', {
            style: 'currency',
            currency: 'IDR',
          })}
        </td>
        <td className="p-5">{i.category}</td>
        <td className="p-5">{i.expiryDate}</td>
        <td className="p-5">
          <Button
            color="red"
            icon={deleteIcon}
            onClick={() => {
              setTextModal(i.name);
              setShowModal(true);
              setIdxDel(i.id);
            }}
            size="small"
            text={'Delete'}
          />
        </td>
      </tr>
    );
  };

  useEffect(() => {
    if (state) {
      inputProduct();
    }
  }, [state]);

  return (
    <div className="bg-white">
      <div className="flex justify-between items-center  px-5 py-10">
        <h6 className="font-semibold text-xl">Products</h6>
        <div className="flex justify-end items-center gap-8">
          <Checkbox
            isChecked={isChecked}
            onChange={onChangeCheck}
            text="Hide expired product"
          />
          <Dropdown
            data={listCategory}
            name="category"
            onChange={onChangeSelect}
            value={category}
          />
          <Button
            color="purple"
            onClick={() => navigate('/products/new')}
            size="medium"
            text="Add New Product"
          />
        </div>
      </div>
      <Table
        columns={columns}
        deleteProduct={deleteProduct}
        isProduct={true}
        items={filteredData}
        rows={rows}
        setShowModal={setShowModal}
        showModal={showModal}
        textModal={textModal}
      />
    </div>
  );
}
