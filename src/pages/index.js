import React, { lazy, Suspense } from 'react';
import LazyFallback from '../components/elements/LazyFallback';

const Suspensed = (Element) =>
  function suspense(props) {
    return (
      <Suspense fallback={<LazyFallback />}>
        <Element {...props} />
      </Suspense>
    );
  };

export const Main = Suspensed(lazy(() => import('./Main')));
export const ProductList = Suspensed(lazy(() => import('./ProductList')));
export const UserList = Suspensed(lazy(() => import('./UserList')));
export const ProductDetail = Suspensed(lazy(() => import('./ProductDetail')));
export const ProductNew = Suspensed(lazy(() => import('./ProductNew')));
