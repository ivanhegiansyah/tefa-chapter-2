import React, { useState, useEffect } from 'react';
import Breadcrumbs from '../../components/elements/Breadcrumbs/Breadcrumbs';
import Input from '../../components/elements/Input/Input';
import Dropdown from '../../components/elements/Dropdown/Dropdown';
import Button from '../../components/elements/Button/Button';
import styles from './styles.scoped.css';
import Checkbox from '../../components/elements/Checkbox/Checkbox';
import { useNavigate } from 'react-router-dom';

export default function ProductNew() {
  const items = [{ name: 'Products' }, { name: 'Add New Product' }];
  const navigate = useNavigate();
  const listCategory = ['Pre-Order', 'Barang Bekas', 'Ready'];

  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState('');
  const [isChecked, setIsChecked] = useState(false);
  const [expiryDate, setExpiryDate] = useState('');
  const [image, setImage] = useState('');
  const [category, setCategory] = useState('');
  const [isDisabled, setIsDisabled] = useState(false);

  const onChangeName = (e) => {
    setName(e.target.value);
  };

  const onChangePrice = (e) => {
    setPrice(e.target.value);
  };

  const onChangeCategory = (e) => {
    setCategory(e.target.value);
  };

  const onChangeDescription = (e) => {
    setDescription(e.target.value);
  };

  const onChangeCheck = () => {
    setIsChecked(!isChecked);
  };

  const onChangeExpiryDate = (e) => {
    setExpiryDate(e.target.value);
  };

  const onChangeImage = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      setImage(e.target.result);
    };
  };

  const onSubmit = () => {
    const newItem = {
      id: Math.floor(Math.random() * 10000),
      name,
      price,
      image,
      description,
      isDeleted: false,
      category,
      expiryDate,
    };
    navigate('/products', { state: newItem });
  };

  useEffect(() => {
    if (isChecked) {
      setIsDisabled(true);
    } else {
      setIsDisabled(false);
    }
  }, [isChecked]);

  return (
    <div className={styles.root}>
      <Breadcrumbs items={items} />
      <div className={styles.horizontal} />
      <div className={styles.container}>
        <div className={styles.kiri}>
          <div className={styles.atas}>
            <Input
              name="product-name"
              onChange={onChangeName}
              placeholder="Enter product name"
              text="Product Name"
              value={name}
              width="medium"
            />
            <Input
              name="product-price"
              onChange={onChangePrice}
              placeholder="Enter product price"
              text="Product Price"
              type="number"
              width="medium"
            />
            <div className={styles['dropdown-label']}>
              <div>Product Category</div>
              <Dropdown
                data={listCategory}
                name="category"
                onChange={onChangeCategory}
                text={'Product Category'}
                value={category}
              />
            </div>
          </div>
          <Input
            area={true}
            name="product-description"
            onChange={onChangeDescription}
            placeholder="Enter product description"
            text="Product Description"
            width="large"
          />
          <Checkbox
            isChecked={isChecked}
            onChange={onChangeCheck}
            text="Product has expiry date"
          />
          <Input
            disabled={isDisabled}
            name="expiry-date"
            onChange={onChangeExpiryDate}
            text="expiry-date"
            type="date"
            width="medium"
          />
        </div>
        <div className={styles.kanan}>
          {image ? (
            <img alt="preview-image" className={styles.circle} src={image} />
          ) : (
            <div className={styles.circle}>Product Image</div>
          )}

          <Input
            accept="image/*"
            labelClassName="image"
            name="image"
            onChange={onChangeImage}
            text="Upload Image"
            type="file"
          />
        </div>
      </div>
      <div className={styles.pojokkanan}>
        <Button color="white" size="small" text="Cancel" />
        <Button color="purple" onClick={onSubmit} size="small" text="Save" />
      </div>
    </div>
  );
}
